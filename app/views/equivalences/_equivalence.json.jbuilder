json.extract! equivalence, :id, :subjects_id, :equivalent_subject_id, :created_at, :updated_at
json.url equivalence_url(equivalence, format: :json)
