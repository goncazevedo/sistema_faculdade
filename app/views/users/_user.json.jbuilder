json.extract! user, :id, :kind, :created_at, :updated_at
json.url user_url(user, format: :json)
