json.extract! subscription, :id, :student_id, :classroom_id, :created_at, :updated_at
json.url subscription_url(subscription, format: :json)
