json.extract! classroom, :id, :professor_id, :subject_id, :period_id, :created_at, :updated_at
json.url classroom_url(classroom, format: :json)
