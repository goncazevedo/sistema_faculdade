class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
    has_one :professor
  mount_uploader :photo, PhotosUploader
  validates :name, presence: true
  enum kind: {
    standard: 0,
    admin: 1,
    professor: 2,
    principal: 3,
    coordinator: 4
    
  }
end
