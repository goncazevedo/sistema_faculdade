class Student < ApplicationRecord
  belongs_to :course
  has_many :courses
  has_many :subscriptions
  has_many :classrooms , through: :subscriptions
  has_many :presences
  has_many :lessons, through: :presences
end
