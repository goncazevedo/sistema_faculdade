class Subject < ApplicationRecord
  belongs_to :department, optional: true
  has_many :course_subjects
  has_many :courses, through: :course_subjects
  has_many :equivalences
  has_many :equivalent_subjects, through: :equivalences, source: :equivalent_subject
end