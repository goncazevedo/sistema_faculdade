class Classroom < ApplicationRecord
  belongs_to :professor
  belongs_to :subject
  belongs_to :period
  has_many :lessons
  has_many :subscriptions
  has_many :students , through: :subscriptions
end