class Department < ApplicationRecord
    has_many :courses
    has_many :subjects
    has_many :professors 
end
