class Professor < ApplicationRecord
  belongs_to :department
  has_many :classrooms
  belongs_to :user, optional: true
end