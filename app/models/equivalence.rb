class Equivalence < ApplicationRecord
  belongs_to :subject
  belongs_to :equivalent_subject, class_name: "Subject"
end
