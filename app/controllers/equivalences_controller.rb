class EquivalencesController < ApplicationController
  before_action :set_equivalence, only: [:show, :edit, :update, :destroy]

  # GET /equivalences
  # GET /equivalences.json
  def index
    @equivalences = Equivalence.all
  end

  # GET /equivalences/1
  # GET /equivalences/1.json
  def show
  end

  # GET /equivalences/new
  def new
    @equivalence = Equivalence.new
  end

  # GET /equivalences/1/edit
  def edit
  end

  # POST /equivalences
  # POST /equivalences.json
  def create

    @equivalence = Equivalence.new(equivalence_params)

    respond_to do |format|
      if @equivalence.save
        format.html { redirect_to request.referrer, notice: 'Equivalence was successfully created.' }
        format.json { render request.referrer, status: :created, location: @equivalence }
      else
        format.html { render :new }
        format.json { render json: @equivalence.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /equivalences/1
  # PATCH/PUT /equivalences/1.json
  def update
    respond_to do |format|
      if @equivalence.update(equivalence_params)
        format.html { redirect_to @equivalence, notice: 'Equivalence was successfully updated.' }
        format.json { render :show, status: :ok, location: @equivalence }
      else
        format.html { render root_path }
        format.json { render json: @equivalence.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /equivalences/1
  # DELETE /equivalences/1.json
  def destroy
    @equivalence.destroy
    respond_to do |format|
      format.html { redirect_to request.referrer, notice: 'Equivalence was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_equivalence
      @equivalence = Equivalence.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def equivalence_params
      params.require(:equivalence).permit(:subject_id, :equivalent_subject_id)
    end
end
