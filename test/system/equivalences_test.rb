require "application_system_test_case"

class EquivalencesTest < ApplicationSystemTestCase
  setup do
    @equivalence = equivalences(:one)
  end

  test "visiting the index" do
    visit equivalences_url
    assert_selector "h1", text: "Equivalences"
  end

  test "creating a Equivalence" do
    visit equivalences_url
    click_on "New Equivalence"

    fill_in "Equivalent subject", with: @equivalence.equivalent_subject_id
    fill_in "Subjects", with: @equivalence.subjects_id
    click_on "Create Equivalence"

    assert_text "Equivalence was successfully created"
    click_on "Back"
  end

  test "updating a Equivalence" do
    visit equivalences_url
    click_on "Edit", match: :first

    fill_in "Equivalent subject", with: @equivalence.equivalent_subject_id
    fill_in "Subjects", with: @equivalence.subjects_id
    click_on "Update Equivalence"

    assert_text "Equivalence was successfully updated"
    click_on "Back"
  end

  test "destroying a Equivalence" do
    visit equivalences_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Equivalence was successfully destroyed"
  end
end
