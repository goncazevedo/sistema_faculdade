require 'test_helper'

class EquivalencesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @equivalence = equivalences(:one)
  end

  test "should get index" do
    get equivalences_url
    assert_response :success
  end

  test "should get new" do
    get new_equivalence_url
    assert_response :success
  end

  test "should create equivalence" do
    assert_difference('Equivalence.count') do
      post equivalences_url, params: { equivalence: { equivalent_subject_id: @equivalence.equivalent_subject_id, subjects_id: @equivalence.subjects_id } }
    end

    assert_redirected_to equivalence_url(Equivalence.last)
  end

  test "should show equivalence" do
    get equivalence_url(@equivalence)
    assert_response :success
  end

  test "should get edit" do
    get edit_equivalence_url(@equivalence)
    assert_response :success
  end

  test "should update equivalence" do
    patch equivalence_url(@equivalence), params: { equivalence: { equivalent_subject_id: @equivalence.equivalent_subject_id, subjects_id: @equivalence.subjects_id } }
    assert_redirected_to equivalence_url(@equivalence)
  end

  test "should destroy equivalence" do
    assert_difference('Equivalence.count', -1) do
      delete equivalence_url(@equivalence)
    end

    assert_redirected_to equivalences_url
  end
end
