class AddUserToProfessor < ActiveRecord::Migration[5.2]
  def change
    add_column :professors, :email, :string
  end
end
