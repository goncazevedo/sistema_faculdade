class CreateClassrooms < ActiveRecord::Migration[5.2]
  def change
    create_table :classrooms do |t|
      t.references :professor, foreign_key: true
      t.references :subject, foreign_key: true
      t.references :period, foreign_key: true
      t.integer :code
      
      t.timestamps
    end
  end
end
