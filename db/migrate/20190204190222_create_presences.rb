class CreatePresences < ActiveRecord::Migration[5.2]
  def change
    create_table :presences do |t|
      t.references :student, foreign_key: true
      t.references :lesson, foreign_key: true

      t.timestamps
    end
  end
end
