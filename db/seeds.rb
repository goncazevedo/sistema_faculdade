# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create!(name:"Admin", email: "admin@admin.com", password: "123456", kind: 1)
15.times do |i|
    User.create!(name: Faker::Pokemon.name, email: Faker::Internet::email, password: "123456", kind:0)
    Department.create!(name: Faker::GameOfThrones.house )
    Period.create!(semester: "#{i}")
end

15.times do |i|
    Professor.create!(name: Faker::HarryPotter.character, department_id: Department.ids.sample)
    Course.create!(name: Faker::Educator.course, department_id: Department.ids.sample)
    Subject.create!(name: Faker::HarryPotter.spell, department_id: Department.ids.sample) 
end

15.times do |i|
    Student.create!(name: Faker::RuPaul.queen, course_id: Course.ids.sample)
    Classroom.create!(professor_id: Professor.ids.sample, subject_id: Subject.ids.sample, period_id: Period.ids.sample, code: rand(100..999))
end