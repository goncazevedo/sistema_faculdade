Rails.application.routes.draw do
  root to: "pages#home"
  scope(path_names: { new: 'novo', edit: 'editar' }) do
    devise_for :users, path: 'usuarios'
    resources :users, path: 'usuarios'
    resources :equivalences, path: 'equivalencias'
    resources :presences, path: "presenças"
    resources :subscriptions, path: "inscriçoes"
    resources :lessons, path: "aulas"
    resources :students, path: "alunos"
    resources :classrooms, path: "turmas"
    resources :periods, path: "periodos"
    resources :subjects, path: "materias"
    resources :courses, path: "cursos"
    resources :professors, path: "professores"
    resources :departments, path: "departamentos"
    resources :course_subjects, path: "materias_do_curso"
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
